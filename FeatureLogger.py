from collections.abc import MutableMapping
import json
import os

class FeatureLogger(MutableMapping):
    """A dictionary that applies an arbitrary key-altering
       function before accessing the keys"""

    def __init__(self, logFileName:str, *args, **kwargs):
        self.store = dict()
        self.logFileName = logFileName
        self.update(dict(*args, **kwargs))  # use the free update to set keys

    def __getitem__(self, key):
        return self.store[key]

    def __setitem__(self, key, value):
        self.store[key] = value
        self.save()

    def __delitem__(self, key):
        del self.store[key]
        self.save()

    def __iter__(self):
        return iter(self.store)
    
    def __len__(self):
        return len(self.store)

    def save(self):
        if not os.path.exists(os.path.dirname(self.logFileName)):
            try:
                os.makedirs(os.path.dirname(self.logFileName))
            except OSError as exc: # Guard against race condition
                if exc.errno != errno.EEXIST:
                    raise
            
            with open(self.logFileName, "w") as f:
                f.write("** Log:"+self.logFileName+"**")
            
        with open(self.logFileName, 'w') as fp:
            json.dump(self.store, fp, indent=4)
    

