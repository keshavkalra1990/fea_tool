#!/bin/bash
# script to extract prsodic features with openSmile for one video
# it creates a temprory directory where feature are extracted and then copy the  csv containing the feature as a .openFace_feature file
# arguments are :
# * directory where the file is
# * name of the file to process

mkdir tmp_feature_dir
# cd tmp_feature_dir
echo $2
csv_filename="${2/'.avi'/'.csv'}"
prosody_filename="${2/'.avi'/'.openSmile_prosodic_feature'}"
echo $wav_filename
echo $csv_filename
echo $prosody_filename
ffmpeg -i $1/$2  -ac 1 tmp_feature_dir/$wav_filename

./external_libs/openSmile/opensmile/build/progsrc/smilextract/SMILExtract -C .//external_libs/openSmile/opensmile/config/audiovisual/audiovideo.conf -I tmp_feature_dir/$wav_filename -O tmp_feature_dir/$csv_filename
# mv tmp_feature_dir/$csv_filename $1/$csv_filename
  
# done
#rm -r tmp_feature_dir
