import platform
import wget
import os
import sys
import zipfile
import shutil

def downloadFile(url, destination_path, unzip):
    os.makedirs(destination_path, exist_ok=True)
    file = wget.download(url, out=destination_path)
    print("\n\nfile : "+file)
    print("destination_path: "+ destination_path)
    if unzip:
        root_name = file.split('/')[-1].replace('.zip','')
        archive = zipfile.ZipFile(file) 
        for file in archive.namelist():
            print("file : "+str(file))
            archive.extract(file, destination_path)
        os.rename(destination_path+root_name, destination_path+"openFace")
    return  destination_path+"openFace"

print(platform.system())
print(platform.release())
print(platform.version())
plt = platform.system()

if plt == "Windows":
    print("Your system is Windows")
    # check 32 or 64
    is_64bits = sys.maxsize > 2**32
    if is_64bits:
        print("64 bits detected")
        url = "https://github.com/TadasBaltrusaitis/OpenFace/releases/download/OpenFace_2.2.0/OpenFace_2.2.0_win_x64.zip"
    else:
        print("32 bits detected")
        url = "https://github.com/TadasBaltrusaitis/OpenFace/releases/download/OpenFace_2.2.0/OpenFace_2.2.0_win_x86.zip"
    cwd = os.getcwd()
    destination_path =  cwd+"\\external_libs\\openFace\\"
    try:
        openface_path = downloadFile(url, destination_path, True)
    except OSError as err:
        print("OS error: {0}".format(err))
    except ValueError:
        print("Could not convert data to an integer.")
    except:
        print("Unexpected error:", sys.exc_info()[0])
        raise
    models = ["https://www.dropbox.com/s/7na5qsjzz8yfoer/cen_patches_0.25_of.dat?dl=1", "https://onedrive.live.com/download?cid=2E2ADA578BFF6E6E&resid=2E2ADA578BFF6E6E%2153072&authkey=AKqoZtcN0PSIZH4", "https://www.dropbox.com/s/k7bj804cyiu474t/cen_patches_0.35_of.dat?dl=1", "https://onedrive.live.com/download?cid=2E2ADA578BFF6E6E&resid=2E2ADA578BFF6E6E%2153079&authkey=ANpDR1n3ckL_0gs", "https://www.dropbox.com/s/ixt4vkbmxgab1iu/cen_patches_0.50_of.dat?dl=1", "https://onedrive.live.com/download?cid=2E2ADA578BFF6E6E&resid=2E2ADA578BFF6E6E%2153074&authkey=AGi-e30AfRc_zvs", "https://www.dropbox.com/s/2t5t1sdpshzfhpj/cen_patches_1.00_of.dat?dl=1", "https://onedrive.live.com/download?cid=2E2ADA578BFF6E6E&resid=2E2ADA578BFF6E6E%2153070&authkey=AD6KjtYipphwBPc"
    ]
    model_destination = openface_path+"\\model\\patch_experts\\"
    for model in models:
        try:
            downloadFile(model, model_destination, False)
        except OSError as err:
            print("OS error: {0}".format(err))
        except ValueError:
            print("Could not convert data to an integer.")
        except:
            print("Unexpected error:", sys.exc_info()[0])
            raise

elif plt == "Linux":
    print("Your system is Linux")
    # do x y z
elif plt == "Darwin":
    print("Your system is MacOS")
    # do x y z
else:
    print("Unidentified system")

