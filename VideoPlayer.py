from PyQt5.QtGui import QIcon, QFont
from PyQt5.QtCore import QDir, Qt, QUrl, QSize
from PyQt5.QtMultimedia import QMediaContent, QMediaPlayer
from PyQt5.QtMultimediaWidgets import QVideoWidget
from PyQt5.QtWidgets import (QApplication, QFileDialog, QHBoxLayout, QLabel, 
        QPushButton, QSizePolicy, QSlider, QStyle, QVBoxLayout, QGridLayout, QWidget, QStatusBar)
from PyQt5 import QtGui
# graph
from pyqtgraph import PlotWidget, plot
import pyqtgraph as pg
import au_graph_widget as agw
# data
from scipy import ndimage
import pandas as pd
import numpy as np

class VideoPlayer(QWidget):

    def __init__(self, parent=None):
        super(VideoPlayer, self).__init__(parent)

        # self.mainWidget = QtGui.QWidget(self) 
        # self.setCentralWidget(self.mainWidget)

        self.XgraphSize = 3000
        self.max_vertical_graph = 6
        
        self.frames = [0]
        self.plots = []
        self.np_data = []
        self.mediaPlayer = QMediaPlayer(None, QMediaPlayer.VideoSurface)

        btnSize = QSize(16, 16)
        videoWidget = QVideoWidget()

        openButton = QPushButton("Open Video")   
        openButton.setToolTip("Open Video File")
        openButton.setStatusTip("Open Video File")
        openButton.setFixedHeight(24)
        openButton.setIconSize(btnSize)
        openButton.setFont(QFont("Noto Sans", 8))
        openButton.setIcon(QIcon.fromTheme("document-open", QIcon("D:/_Qt/img/open.png")))
        openButton.clicked.connect(self.abrir)

        self.playButton = QPushButton()
        self.playButton.setEnabled(False)
        self.playButton.setFixedHeight(24)
        self.playButton.setIconSize(btnSize)
        self.playButton.setIcon(self.style().standardIcon(QStyle.SP_MediaPlay))
        self.playButton.clicked.connect(self.play)

        self.positionSlider = QSlider(Qt.Horizontal)
        self.positionSlider.setRange(0, 0)
        self.positionSlider.sliderMoved.connect(self.setPosition)

        self.statusBar = QStatusBar()
        self.statusBar.setFont(QFont("Noto Sans", 7))
        self.statusBar.setFixedHeight(14)

        controlLayout = QHBoxLayout()
        controlLayout.setContentsMargins(0, 0, 0, 0)
        controlLayout.addWidget(openButton)
        controlLayout.addWidget(self.playButton)
        controlLayout.addWidget(self.positionSlider)

        video_layout = QVBoxLayout()
        video_layout.addWidget(videoWidget)
        video_layout.addLayout(controlLayout)
        video_layout.addWidget(self.statusBar)
        self.video_layout = video_layout

        graph_layout = QGridLayout()
        self.graph_layout = graph_layout
        self.graph_init_value = 0

        over_graph_layout = QVBoxLayout()
        self.graph_sizeSlider = QSlider(Qt.Horizontal)
        self.graph_sizeSlider.setRange(0, 1000)
        self.graph_sizeSlider.sliderMoved.connect(self.setGraphSize)
        over_graph_layout.addLayout(graph_layout)
        over_graph_layout.addWidget(self.graph_sizeSlider)


        main_layout = QHBoxLayout()
        main_layout.addLayout(video_layout)
        main_layout.addLayout(over_graph_layout)
        self.setLayout(main_layout)

        self.mediaPlayer.setVideoOutput(videoWidget)
        self.mediaPlayer.stateChanged.connect(self.mediaStateChanged)
        self.mediaPlayer.positionChanged.connect(self.positionChanged)
        self.mediaPlayer.durationChanged.connect(self.durationChanged)
        self.mediaPlayer.error.connect(self.handleError)
        self.mediaPlayer.setNotifyInterval(10)
        self.statusBar.showMessage("Ready")

    def abrir(self):
        self.frames = [0]
        self.plots = []
        self.np_data = []
        self.graph_init_value = 0
        video_fileName, _ = QFileDialog.getOpenFileName(self, "Video file to display",
                "../data/Semaine-db/Semaine-Db-Sept2015/Sessions/", "Video Files (*.mp4 *.flv *.ts *.mts *.avi)")
        # video_fileName = 
        if video_fileName != '':
            self.mediaPlayer.setMedia(
                    QMediaContent(QUrl.fromLocalFile(video_fileName)))
            self.playButton.setEnabled(True)
            self.statusBar.showMessage(video_fileName)
            # process video file feature
            self.checkFeature(video_fileName)
            
            self.play()

    def checkFeature(self, video_fileName):
        installers_data_json = pd.read_json('installers_data.json')
        features = installers_data_json.columns
        df = pd.DataFrame()
        for feature in features:
            feature_dict = installers_data_json[feature]["execution_script"][0]
            df = self.processFeature(df, video_fileName, feature_dict)
        self.processToGraph(df)

    def processFeature(self, df:pd.DataFrame(), video_fileName: str, feature_dict: dict):
        print(">>>> video_fileName :: "+str(video_fileName))
        video_extension = video_fileName.split('.')[-1]
        print(">>>> video_extension :: "+str(video_extension))
        feature_fileName = video_fileName.replace(video_extension, feature_dict["name"])
        print(">>>> feature_fileName :: "+str(feature_fileName))
        data = pd.read_csv(feature_fileName)
        print("df ::: "+str(df.head()))
        print("data ::: "+str(data.head()))
        data = data[list(feature_dict["features"][0].keys())]
        # col_to_drop = [c for c in data.columns if c in ["frame", "timestamp"]]
        # data.drop(columns = col_to_drop, inplace=True)
        if len(df)==0:
            df = data
        else:
            df = pd.merge_asof(df, data, on='frame', direction='nearest')
        return df

    def processToGraph(self, data:pd.DataFrame()):
        columns = data.columns
        np_data = data.values
        print('np_data : '+str(np_data.shape))
        self.np_data.append(ndimage.gaussian_filter1d(np_data, 5, 0)    )
        self.plots = []
        self.lines = []
        self.frames = (data['timestamp'].values)*1000.0
        self.graph_sizeSlider.setRange(0, len(self.frames))
        for i in range(len(columns)) : 
            position = [(self.graph_init_value+i)%self.max_vertical_graph, int((self.graph_init_value+i)/self.max_vertical_graph)]
            graph_widget = agw.au_graph_widget(columns[i], self.np_data[-1][:, i], self.frames, self.XgraphSize)
            self.graph_layout.addWidget(graph_widget, *position)
            self.plots.append(graph_widget)
        self.graph_init_value +=len(columns) 
            
            
    def play(self):
        if self.mediaPlayer.state() == QMediaPlayer.PlayingState:
            self.mediaPlayer.pause()
        else:
            self.mediaPlayer.play()

    def mediaStateChanged(self, state):
        if self.mediaPlayer.state() == QMediaPlayer.PlayingState:
            self.playButton.setIcon(
                    self.style().standardIcon(QStyle.SP_MediaPause))
        else:
            self.playButton.setIcon(
                    self.style().standardIcon(QStyle.SP_MediaPlay))

    def positionChanged(self, position):
        self.positionSlider.setValue(position)
        x_min_value = max(0, min(position-int(self.XgraphSize/2), self.frames[-1]-self.XgraphSize))

        x_max_value = min(max(self.XgraphSize, position+int(self.XgraphSize/2)), self.frames[-1])

        
        for i in range(len(self.plots)) : 
            self.plots[i].positionChanged(position)

    def durationChanged(self, duration):
        self.positionSlider.setRange(0, duration)

    def setPosition(self, position):
        self.mediaPlayer.setPosition(position)
        
    def setGraphSize(self, position):
        self.XgraphSize = position
        for i in range(len(self.plots)) : 
            self.plots[i].setXgraphSize(self.XgraphSize)


    def handleError(self):
        self.playButton.setEnabled(False)
        self.statusBar.showMessage("Error: " + self.mediaPlayer.errorString())
